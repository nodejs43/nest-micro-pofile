import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ProfileModule } from './profile/profile.module';

@Module({
  imports: [
    ProfileModule,
    MongooseModule.forRoot(
      `mongodb+srv://fedia97:fedia97@cluster0.05biu.mongodb.net/profile?retryWrites=true&w=majority`,
    ),
  ],
})
export class MainModule {}
