import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Profile, ProfileDocument } from './schemas/profile.schema';
import { UpdateProfileDto } from './dto/update-profile.dto';
import { CreateProfileDto } from './dto/create-profile.dto';

@Injectable()
export class ProfileService {
  constructor(
    @InjectModel(Profile.name) private profileModel: Model<ProfileDocument>,
  ) {}

  async getAll(): Promise<Profile[]> {
    return this.profileModel.find().exec();
  }

  async getById(id: string): Promise<Profile> {
    return this.profileModel.findById(id);
  }

  async create(profileDto: CreateProfileDto): Promise<Profile> {
    const newProduct = new this.profileModel(profileDto);
    return newProduct.save();
  }

  async remove(id: string): Promise<Profile> {
    return this.profileModel.findByIdAndRemove(id);
  }

  async update(id: string, profileDto: UpdateProfileDto): Promise<Profile> {
    return this.profileModel.findByIdAndUpdate(id, profileDto, { new: true });
  }
}
